ns_log Debug "im_trans_status_component($user_id, $project_id)"
set current_user_id [ad_get_user_id]
set current_user_is_employee_p [expr [im_user_is_employee_p $current_user_id] | [im_is_user_site_wide_or_intranet_admin $current_user_id]]
set return_url [export_vars -base "/intranet/projects/view" -url {project_id}]

# Is this a translation project?
if {![im_project_has_type $project_id "Translation Project"]} {
    ns_log Debug "im_task_status_component: Project $project_id is not a translation project"
    return ""
}
    
im_project_permissions $current_user_id $project_id view read write admin
    if {![im_permission $current_user_id view_trans_task_status]} {
    return ""
}
    
set bgcolor(0) " class=roweven"
set bgcolor(1) " class=rowodd"
    
set up [db_string upload_action_id "select category_id from im_categories where category_type='Intranet File Action Type' and lower(category)='upload'" -default ""]
set down [db_string download_action_id "select category_id from im_categories where category_type='Intranet File Action Type' and lower(category)='download'" -default ""]
    
set task_type_list [db_string project_info "select aux_string1 from im_projects p, im_categories c where c.category_id = p.project_type_id and p.project_id = :project_id" -default ""]
    
# ------------------Display the list of current tasks...-------------
    
set task_status_html "
    <tr>
      <td class=rowtitle align=center rowspan=2>[_ intranet-translation.Name]</td>
      <td class=rowtitle align=center rowspan=2>[_ intranet-translation.Target_Language]</td>"
    
set unassigned_select_sql [list]

foreach type $task_type_list {
    set type_id [db_string task_type "select category_id from im_categories where lower(category) = :type and category_type = 'Intranet Trans Task Type'"]
    append task_status_html "<td class=rowtitle align=center colspan=3>[im_category_from_id $type_id]</td>\n"
    lappend unassigned_select_sql "count(t.$type) as unassigned_$type"
    lappend unassigned_select_sql "CASE WHEN sum(t.$type) is null THEN 0 ELSE sum(t.$type) END as unassigned_${type}_wc"

}

append task_status_html "
  <td class=rowtitle align=center colspan=[llength $task_type_list]>[_ intranet-translation.Wordcount]</td>
</tr>
<tr>"

set col_count 2
foreach type $task_type_list {
    set col_count [expr $col_count +3]

    append task_status_html "
      <td class=rowtitle align=center>[_ intranet-translation.Ass]</td>
      <td class=rowtitle align=center>[_ intranet-translation.Dn]</td>
        <td class=rowtitle align=center>[_ intranet-translation.Up]</td>
    "
}

foreach type $task_type_list {
    incr col_count
    set type_id [db_string task_type "select category_id from im_categories where lower(category) = :type and category_type = 'Intranet Trans Task Type'"]
    append task_status_html "<td class=rowtitle align=center>[im_category_from_id $type_id]</td>"
}

append task_status_html "</tr>\n"

set task_status_html "<form action=/intranet-translation/trans-tasks/task-action method=POST>
    [export_form_vars project_id return_url]
    
    <table cellpadding=0 cellspacing=2 border=0>
    <tr>
      <td class=rowtitle align=center colspan=$col_count>
        [_ intranet-translation.lt_Project_Workflow_Stat]
    [im_gif -translate_p 1 help "Shows the status of all tasks\nAss: Assigned Files\nDn: Downloaded Files\nUp: Uploaded Files"]
      </td>
    </tr>
    $task_status_html"
    
# ------------------- Get the number of tasks to assign----------------
# This SQL calculates the overall number of files/wordcounts to be
# assigned. We are going to subtract the assigned files/wcs from it.
if {$unassigned_select_sql ne ""} {
    set unassigned_files_sql "
    select
        [join $unassigned_select_sql ","]
        from
            (select
                t.task_type_id,
                CASE WHEN t.task_type_id in (select category_id from im_categories where aux_string1 like '%trans%' and category_type = 'Intranet Project Type') THEN t.task_units END as trans,
                CASE WHEN t.task_type_id in (select category_id from im_categories where aux_string1 like '%proof%' and category_type = 'Intranet Project Type') THEN t.task_units END as proof,
                CASE WHEN t.task_type_id in (select category_id from im_categories where aux_string1 like '%edit%' and category_type = 'Intranet Project Type') THEN t.task_units END as edit,
                CASE WHEN t.task_type_id in (select category_id from im_categories where aux_string1 like '%copy%' and category_type = 'Intranet Project Type') THEN t.task_units END as copy,
                CASE WHEN t.task_type_id in (select category_id from im_categories where aux_string1 like '%other%' and category_type = 'Intranet Project Type') THEN t.task_units END as other
            from
                im_trans_tasks t
            where
                t.project_id = :project_id
            ) t
    "

    db_1row unassigned_totals $unassigned_files_sql
}
# ----------------------Get task status ------------------------------
    
# Aggregate the information from the inner_sql and
# order it by user
set task_status_sql "
    select
        u.user_id,
        sum(trans_down) as trans_down,
        sum(trans_up) as trans_up,
        sum(edit_down) as edit_down,
        sum(edit_up) as edit_up,
        sum(proof_down) as proof_down,
        sum(proof_up) as proof_up,
        sum(other_down) as other_down,
        sum(other_up) as other_up
    from
        users u,
        acs_rels r,
        (select distinct
            t.task_id,
            u.user_id,
            CASE WHEN u.user_id = t.trans_id and action_type_id=:down THEN 1 END as trans_down,
            CASE WHEN u.user_id = t.trans_id and action_type_id=:up THEN 1 END as trans_up,
            CASE WHEN u.user_id = t.edit_id and action_type_id=:down THEN 1 END as edit_down,
            CASE WHEN u.user_id = t.edit_id and action_type_id=:up THEN 1 END as edit_up,
            CASE WHEN u.user_id = t.proof_id and action_type_id=:down THEN 1 END as proof_down,
            CASE WHEN u.user_id = t.proof_id and action_type_id=:up THEN 1 END as proof_up,
            CASE WHEN u.user_id = t.other_id and action_type_id=:down THEN 1 END as other_down,
            CASE WHEN u.user_id = t.other_id and action_type_id=:up THEN 1 END as other_up
        from
            users u,
            acs_rels r,
            im_trans_tasks t,
            im_task_actions a
        where
            r.object_id_one = :project_id
            and r.object_id_one = t.project_id
            and u.user_id = r.object_id_two
            and (	u.user_id = t.trans_id
                or u.user_id = t.edit_id
                or u.user_id = t.proof_id
                or u.user_id = t.other_id)
            and a.user_id = u.user_id
            and a.task_id = t.task_id
        ) t
    where
        r.object_id_one = :project_id
        and r.object_id_two = u.user_id
        and u.user_id = t.user_id
    group by
        u.user_id
    "
    
# ----- Get the absolute number of tasks by project phase ---------------

set task_filecount_sql "
    select
        t.user_id,
        min(t.target_language_id) as target_language_id,
        count(trans_ass) as trans_ass,
        count(edit_ass) as edit_ass,
        count(proof_ass) as proof_ass,
        count(other_ass) as other_ass,
        count(copy_ass) as copy_ass,
        sum(trans_ass) as trans_words,
        sum(edit_ass) as edit_words,
        sum(proof_ass) as proof_words,
        sum(other_ass) as other_words,
        sum(copy_ass) as copy_words
    from
        (select
            u.user_id,
            t.task_id,
            t.target_language_id,
            CASE WHEN u.user_id = t.trans_id THEN t.task_units END as trans_ass,
            CASE WHEN u.user_id = t.edit_id THEN t.task_units END as edit_ass,
            CASE WHEN u.user_id = t.proof_id THEN t.task_units END as proof_ass,
            CASE WHEN u.user_id = t.other_id THEN t.task_units END as other_ass,
            CASE WHEN u.user_id = t.copy_id THEN t.task_units END as copy_ass
        from
            users u,
            acs_rels r,
            im_trans_tasks t
        where
            r.object_id_one = :project_id
            and r.object_id_one = t.project_id
            and u.user_id = r.object_id_two
            and (
                u.user_id = t.trans_id
                or u.user_id = t.edit_id
                or u.user_id = t.proof_id
                or u.user_id = t.other_id
            )
        ) t
    group by t.user_id

"
    
set task_sql "
    select
        u.user_id,
        im_name_from_user_id (u.user_id) as user_name,
        im_name_from_id(c.target_language_id) as target_language,
        target_language_id,
        CASE WHEN c.trans_ass is null THEN 0 ELSE c.trans_ass END as trans_ass,
        CASE WHEN c.edit_ass is null THEN 0 ELSE c.edit_ass END as edit_ass,
        CASE WHEN c.proof_ass is null THEN 0 ELSE c.proof_ass END as proof_ass,
        CASE WHEN c.other_ass is null THEN 0 ELSE c.other_ass END as other_ass,
        CASE WHEN c.copy_ass is null THEN 0 ELSE c.other_ass END as copy_ass,
        CASE WHEN c.trans_words is null THEN 0 ELSE c.trans_words END as trans_words,
        CASE WHEN c.edit_words is null THEN 0 ELSE c.edit_words END as edit_words,
        CASE WHEN c.proof_words is null THEN 0 ELSE c.proof_words END as proof_words,
        CASE WHEN c.other_words is null THEN 0 ELSE c.other_words END as other_words,
        CASE WHEN c.copy_words is null THEN 0 ELSE c.other_words END as copy_words,
        s.trans_down,
        s.trans_up,
        s.edit_down,
        s.edit_up,
        s.proof_down,
        s.proof_up,
        s.other_down,
        s.other_up
    from
        acs_rels r,
        users u left outer join ($task_status_sql) s on (u.user_id = s.user_id)
        left outer join ($task_filecount_sql) c on (u.user_id = c.user_id)
    where
        r.object_id_one = :project_id
        and r.object_id_two = u.user_id
"
    
# --------------------- Display the results ----------------------
    
set ctr 1

db_foreach task_status_sql $task_sql {
    
    # subtract the assigned files from the unassigned
    set assigned_at_all_p 0
    foreach type $task_type_list {
        set unassigned_${type} [expr [set unassigned_${type}] - [set ${type}_ass]]
        set unassigned_${type}_wc [expr [set unassigned_${type}_wc] - [set ${type}_words]]
        if {0 != [set ${type}_ass]} {
            set assigned_at_all_p 1
        }
    }
    
    if {$assigned_at_all_p} {
        append task_status_html "
        <tr $bgcolor([expr $ctr % 2])>
          <td>\n"
        
        if {$current_user_is_employee_p} {
            append task_status_html "<A HREF=/intranet/users/view?user_id=$user_id>$user_name</A>\n"
        } else {
            append task_status_html "User# $ctr\n"
        }
        append task_status_html "</td><td>$target_language</td>"
        
        foreach type $task_type_list {
            
            if {[set ${type}_ass] eq 0} {
                # Not assigned to this type, so don't bother with the further logic
                append task_status_html "
                    <td></td>
                    <td></td>
                    <td></td>
                "
            } else {
                # Get the deadline for the user for this type
                db_1row deadline "select min(${type}_end_date) as deadline, case when min(${type}_end_date) < now() then 1 else 0 end as overdue_p, case when to_char(min(${type}_end_date),'YYYY-MM-DD') = to_char(now(),'YYYY-MM-DD') then 1 else 0 end as due_today_p from im_trans_tasks where project_id = :project_id and ${type}_id = :user_id"
                
                set package_file ""
                # Check if we have files using a callback
                callback intranet-translation::task_status_package_file \
                    -type $type -user_id $user_id -project_id $project_id -target_language_id $target_language_id
                    
                # Set the background color coding
                set task_bgcolor ""
                if {[set ${type}_up] ne ""} {
                    # Uploaded, so we are fine
                    set task_bgcolor "bgcolor=green"
                } elseif {$overdue_p} {
                    set task_bgcolor "bgcolor=red"
                } elseif {$due_today_p} {
                    set task_bgcolor "bgcolor=yellow"
                }
                
                # If the source file is present, make down a link to E-Mail the file to the assignee
                set up_html ""
                set down_html ""
                if {$package_file ne ""} {
                    # Check if the package was already downloaded
                    if {[set ${type}_down] ne ""} {
                        # Downloaded, so provide an upload link
                        set up_html  "<a href='[export_vars -base "/intranet-translation/trans-tasks/task-package-upload" -url {type user_id project_id package_file target_language {new_status_p 0}}]'><img src='/intranet/images/zip-upload.gif'/></a>"
                        # It was downloaded, check if it has already been uploaded
                        if {[set ${type}_up] ne ""} {
                            set up_html  "<a href='[export_vars -base "/intranet-translation/trans-tasks/task-package-upload" -url {type user_id project_id package_file target_language {new_status_p 0}}]'><img src='/intranet/images/zip-upload.gif'/></a>"
                            #provide the link to download the package
                            append up_html "&nbsp;<a href='[export_vars -base "/intranet-translation/trans-tasks/task-package-download" -url {type user_id project_id package_file {return_package_p 1}}]'><img src='/intranet/images/navbar_default/arrow_comp_down.png'/></a>"
                        }
                    }
                    # Check if we send the email already
                    set mail_log_id [db_string mail_log "select ml.log_id from acs_mail_log ml, acs_mail_log_recipient_map lrm where ml.log_id = lrm.log_id and filesystem_files = '[list "$package_file"]' and recipient_id = :user_id" -default ""]
                    if {[set ${type}_down] ne ""} {
                        # File already downloaded, don't change the status
                        set new_status_p 0
                    } else {
                        set new_status_p 1
                    }
                    if {$mail_log_id ne ""} {
                        set down_html "<a href='[export_vars -base "/intranet-mail/one-message" -url {{log_id $mail_log_id} return_url}]'><img src='/intranet/images/navbar_default/email_link.png'/></a>"
                    } else {
                        set down_html "<a href='[export_vars -base "/intranet-translation/trans-tasks/task-package-mail" -url {type user_id project_id package_file return_url new_status_p}]'><img src='/intranet/images/navbar_default/email_go.png'/></a>"
                    }
                    append down_html "&nbsp;<a href='[export_vars -base "/intranet-translation/trans-tasks/task-package-download" -url {type user_id project_id package_file new_status_p}]'><img src='/intranet/images/zip-download.gif'/></a>"
                }
                append task_status_html "
                    <td $task_bgcolor>[lc_time_fmt $deadline "%x %X"]</td>
                    <td>$down_html</td>
                    <td>$up_html</td>
                "
            }
        }
        
        foreach type $task_type_list {
            append task_status_html "<td>[set ${type}_words]</td>"
        }
        append task_status_html "</tr>"
        incr ctr
    }
}
    
    
append xxx_task_status_html "<tr $bgcolor([expr $ctr % 2])>
    <td>unassigned tasks</td>"

foreach type $task_type_list {
    append xxx_task_status_html "
    <td>[set unassigned_${type}]</td>
    <td></td>
    <td></td>"
}

foreach type $task_type_list {
    append xxx_task_status_html "<td>[expr round([set unassigned_${type}_wc])]</td>"
}

append xxx_task_status_html "</tr>"
    
append task_status_html "<tr><td colspan=$col_count align=left>"
    
if {[im_permission $current_user_id "view_trans_tasks"]} {
    append task_status_html "<input type=submit value='[_ intranet-translation.View_Tasks]' name=submit_view>\n"
    append task_status_html "<input type=submit value='[_ intranet-translation.Assign_Tasks]' name=submit_assign>\n"
}
    
append task_status_html "
  </td>
</tr>
\n</table>\n</form>\n\n"
    

# Update Project Advance Percentage
im_trans_task_project_advance $project_id
