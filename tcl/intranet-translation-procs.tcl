# /packages/intranet-translation/tcl/intranet-translation-procs.tcl
#
# Copyright (C) 2004-2009 ]project-open[
#
# All rights reserved (this is not GPLed software!).
# Please check http://www.project-open.com/ for licensing
# details.

ad_library {
    Specific stuff for translation handling and  PO creation

    @author malte.sussdorff@cognovis.de
    
}

ad_proc -public im_package_translation_id {} {
    Returns the package id of the intranet-translation package
} {
    return [util_memoize im_package_translation_id_helper]
}

ad_proc -private im_package_translation_id_helper {} {
    return [db_string im_package_core_id {
        select package_id from apm_packages
        where package_key = 'intranet-translation'
    } -default 0]
}


ad_proc -public im_translation_create_purchase_orders {
    -project_id
    {-target_cost_type_id 0}
    {-target_cost_status_id 0}
    {-cost_center_id ""}
    {-rate_array ""}
    {-freelancer_id ""}
} {
    Create the purchase orders for a project or all tasks which do not have a
    purchase order created yet
    
    @param rate_array Array in the form ${task_type_id}_${task_uom_id}_${freelance_id} for the key and the actual rate for this combination as the value
} {
    # ---------------------------------------------------------------
    # Defaults & Security
    # ---------------------------------------------------------------

    set user_id [ad_maybe_redirect_for_registration]

    if {0 == $target_cost_status_id} { set target_cost_status_id [im_cost_status_created] }
    if {0 == $target_cost_type_id} { set target_cost_type_id [im_cost_type_po] }

    set cost_status_id $target_cost_status_id

    set todays_date [db_string get_today "select to_char(sysdate,'YYYY-MM-DD') from dual"]

    set user_locale [lang::user::locale]
    set locale $user_locale

    set im_material_default_translation_material_id [im_material_default_translation_material_id]

    # ---------------------------------------------------------------
    # Get a list of all freelancers working on this project
    # ---------------------------------------------------------------

    set subprojects [db_list subprojects "
        select	children.project_id
        from	im_projects parent,
            im_projects children
        where
            children.project_status_id not in ([join [im_sub_categories [im_project_status_closed]] ","])
            and children.tree_sortkey
                between parent.tree_sortkey
                and tree_right(parent.tree_sortkey)
            and parent.project_id = :project_id
        union
        select :project_id from dual
    "]


    lappend subprojects 0
    set subproject_sql "([join $subprojects ", "])"

    if {$freelancer_id ne ""} {
        set provider_where "and pe.person_id = :freelancer_id"
    } else {
        set provider_where ""
    }

    set type_sql_list [list]
    db_foreach category "select category, category_id, aux_int2 from im_categories where category_type = 'Intranet Trans Task Type'" {
        if {$aux_int2 ne ""} {
            set type [string tolower $category]
            
            lappend type_sql_list "
            select
                tt.${type}_id as freelance_id,
                '$type' as action,
                ${type}_end_date as delivery_date,
                end_date,
                $aux_int2 as po_task_type_id,
                im_category_from_id($aux_int2) as po_task_type,
                tt.*
              from	im_trans_tasks tt
              where	tt.project_id in $subproject_sql
                and tt.${type}_id is not null
            "
        }
    }
    set tt_sql [join $type_sql_list "\n UNION \n"]
    set task_sql "
    select distinct
        pe.person_id as freelance_id,
        im_name_from_user_id (pe.person_id) as freelance_name,
        im_category_from_id(m.object_role_id) as role,
        im_category_from_id(tt.source_language_id) as source_language,
        im_category_from_id(tt.target_language_id) as target_language,
        p.subject_area_id,
        im_category_from_id(tt.task_uom_id) as task_uom,
        im_category_from_id(tt.task_status_id) as task_status,
        tt.*
    from
        acs_rels r,
        im_biz_object_members m,
        (
            $tt_sql
        ) tt,
        persons pe,
        im_projects p,
        group_distinct_member_map fmem
    where
        r.object_id_one = p.project_id
        and p.project_id in $subproject_sql
        and r.rel_id = m.rel_id
        and r.object_id_two = pe.person_id
        and fmem.group_id = [im_freelance_group_id]
        and pe.person_id = fmem.member_id
        and pe.person_id = tt.freelance_id
        $provider_where
    order by
        tt.freelance_id
    "


    # ---------------------------------------------------------------
    # Get the billable units and units of measure
    # ---------------------------------------------------------------

    set editing_words_per_hour [ad_parameter -package_id [im_package_freelance_invoices_id] "EditingWordsPerHour" "" 1000]
    if {$editing_words_per_hour ne 1} {
        # We obviously want to keep the word count
        set action "hourly_edit"
    }
    
    set freelance_ids [list]

    db_foreach task_tasks $task_sql {

       set file_type_id ""
       set task_type_id $po_task_type_id
       set material_id [im_material_create_from_parameters -material_uom_id $task_uom_id]

        set po_created_p [db_string po_created "select i.invoice_id from im_invoice_items ii, im_invoices i where task_id = :task_id and company_contact_id = :freelance_id and item_material_id = :material_id and i.invoice_id = ii.invoice_id limit 1" -default 0]
        if {!$po_created_p} {
            lappend po_task_ids($freelance_id) $task_id
            if {[lsearch $freelance_ids $freelance_id]<0} {
                lappend freelance_ids $freelance_id

                set provider_id [db_string select_company {
                    select  c.company_id as provider_id
                    from	    acs_rels r,
                            im_companies c
                    where	r.object_id_one = c.company_id
                    and     r.object_id_two = :freelance_id
                    limit 1
                } -default 0]
                set provider($freelance_id) $provider_id
            }

            # ---------------------------------------------------------------
            # Get the billable units
            # ---------------------------------------------------------------

            switch $action {
                trans - edit - proof {
                    array set provider_matrix [im_trans_trados_matrix -task_type $action $provider_id]

                    db_1row billable_units "
                                        select round((tt.match_x * $provider_matrix(x) +
                                                tt.match_rep * $provider_matrix(rep) +
                                                tt.match_perf * $provider_matrix(perf) +
                                                tt.match_cfr * $provider_matrix(cfr) +
                                                tt.match100 * $provider_matrix(100) +
                                                tt.match95 * $provider_matrix(95) +
                                                tt.match85 * $provider_matrix(85) +
                                                tt.match75 * $provider_matrix(75) +
                                                tt.match50 * $provider_matrix(50) +
                                                tt.match0 * $provider_matrix(0) +
                                                tt.match_f95 * $provider_matrix(f95) +
                                                tt.match_f85 * $provider_matrix(f85) +
                                                tt.match_f75 * $provider_matrix(f75) +
                                                tt.match_f50 * $provider_matrix(f50) +
                                                tt.locked * $provider_matrix(locked)
                                            ),0)   as po_billable_units,
                                            tt.task_uom_id as po_task_uom_id
                                        from im_trans_tasks tt
                                        where task_id = :task_id
                                        and (
                                                tt.task_uom_id = [im_uom_s_word]
                                                and tt.match100 is not null
                                            )
                                        UNION
                                        select tt.task_units as po_billable_units,
                                               tt.task_uom_id as po_task_uom_id
                                        from	im_trans_tasks tt
                                        where task_id = :task_id
                                        and (
                                            tt.task_uom_id != [im_uom_s_word]
                                            or tt.match100 is null
                                        )
                                    "
                }
                hourly_edit {
                    set action "edit"
                    db_1row billable_units "
                            select (	tt.match_x +
                                    tt.match_rep +
                                    tt.match_perf +
                                    tt.match_cfr +
                                    tt.match100 +
                                    tt.match95 +
                                    tt.match85 +
                                    tt.match75 +
                                    tt.match50 +
                                    tt.match0 +
                                    tt.match_f95 +
                                    tt.match_f85 +
                                    tt.match_f75 +
                                    tt.match_f50

                                ) / $editing_words_per_hour as po_billable_units,
                            [im_uom_hour] as po_task_uom_id
                        from	   im_trans_tasks tt
                        where   tt.task_id = :task_id
                            and (
                                tt.task_uom_id = [im_uom_s_word]
                                and tt.match100 is not null
                            )
                    UNION
                        select tt.task_units as po_billable_units,
                            tt.task_uom_id as po_task_uom_id
                        from	im_trans_tasks tt
                        where	tt.task_id = :task_id
                            and (
                                tt.task_uom_id != [im_uom_s_word]
                                or tt.match100 is null
                            )
                    "
                }
                default {
                    db_1row billable_units "
                            select  tt.billable_units as po_billable_units,
                                    tt.task_uom_id as po_task_uom_id
                            from    im_trans_tasks tt
                            where   tt.task_id = :task_id"
                }
            }
            if {[info exists task_types(${task_id}_${freelance_id})]} {
                lappend task_types(${task_id}_${freelance_id}) $po_task_type_id
            } else {
                set task_types(${task_id}_${freelance_id}) [list $po_task_type_id]
            }
            
            # We need the tasks per freelancer for the minimum price
            # Pray we don't assign one freelancer to multiple language
            # Combinations....
            # Then we would have to differentiate by the task-types and
            # The target language
            
            if {[info exists task_type_ids(${freelance_id})]} {
                lappend task_type_ids(${freelance_id}) $po_task_type_id
            } else {
                set task_type_ids(${freelance_id}) [list $po_task_type_id]
            }
            set source_language_freelancer($freelance_id) $source_language_id
            set target_language_freelancer($freelance_id) $target_language_id

            if {[info exists billable_units_total(${po_task_type_id}_${freelance_id})]} {
                set billable_units_total(${po_task_type_id}_$freelance_id) [expr [set billable_units_total(${po_task_type_id}_$freelance_id)] + $po_billable_units]
            } else {
                set billable_units_total(${po_task_type_id}_$freelance_id) $po_billable_units
            }
            
            set create_line_item(${task_id}_${po_task_type_id}_${freelance_id}) 1
            set billable_units_task(${task_id}_${po_task_type_id}_${freelance_id}) $po_billable_units


            set uom(${task_id}_${po_task_type_id}_${freelance_id}) $po_task_uom_id

            set subject_area($task_id) $subject_area_id
            set source_language_task($task_id) $source_language_id
            set target_language_task($task_id) $target_language_id

            # Title is there - add specifics
            switch $po_task_type_id {
                86 {
                    set task_date_pretty [lc_time_fmt $other_end_date "%x %X" $locale]
                    set end_date $other_end_date
                }
                88 {
                    set task_date_pretty [lc_time_fmt $edit_end_date "%x %X" $locale]
                    set end_date $edit_end_date
                }
                93 {
                    set task_date_pretty [lc_time_fmt $trans_end_date "%x %X" $locale]
                    set end_date $trans_end_date
                }
                95 {
                    set task_date_pretty [lc_time_fmt $proof_end_date "%x %X" $locale]
                    set end_date $proof_end_date
                }
                default {set task_date_pretty [lc_time_fmt $end_date "%x %X" $locale]}
            }

            set task_title(${task_id}_${po_task_type_id}_${freelance_id}) "$po_task_type: $task_name ($source_language -> $target_language) Deadline: \"$task_date_pretty CET\""

            # Maybe we need the end date in the purchase order later
            set task_end_date(${task_id}_${po_task_type_id}_${freelance_id}) $end_date

        }
    }

    # ---------------------------------------------------------------
    # Loop through each freelancer to create the purchase orders
    # ---------------------------------------------------------------

    set created_invoice_ids [list]

    # Check if we have a rate array which would not get the rates from the database
    if {"" != $rate_array} {
        upvar $rate_array rates
    }

    foreach freelance_id $freelance_ids {

        # ---------------------------------------------------------------
        # If we have tasks needing a purchase order, create the order first
        # ---------------------------------------------------------------

        if {[llength $po_task_ids($freelance_id)] == 0} {continue}

        # create the purchase order
        set invoice_nr [im_next_invoice_nr -cost_type_id $target_cost_type_id -cost_center_id $cost_center_id]
        set invoice_id [im_new_object_id]
        set company_id [im_company_internal]
        set provider_id $provider($freelance_id)

        # Check if we have a company for this freelancer, otherwise we can't create the purchase order
        if { 0 == $provider_id } { continue }

        db_1row select_company {
                select  company_id as provider_id,
                        default_vat,
                        default_tax,
                        default_payment_method_id,
                        default_po_template_id,
                        payment_term_id,
                        vat_type_id
                from	    im_companies c
                where	company_id = :provider_id
        }


        if {"" == $default_po_template_id} {
            # Get a sensible default
            set template_id [db_string internal_template "select default_po_template_id from im_companies where company_id = :company_id" -default ""]
        } else {
            set template_id $default_po_template_id
        }

        # Get the payment days from the company
        set payment_days [ad_parameter -package_id [im_package_cost_id] "DefaultCompanyInvoicePaymentDays" "" 30]
        set tax_format [im_l10n_sql_currency_format -style simple]
        set note ""

        # Find the currency for the provider by looking at the prices
        set currency ""
        if {[im_table_exists "im_trans_prices"]} {
            db_0or1row currency "select currency, count(*) as num_prices from im_trans_prices where company_id = :provider_id group by currency order by num_prices desc limit 1"
        }
        
        if {$currency eq ""} {
            set currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
        }



        db_transaction {
            set invoice_id [db_exec_plsql create_invoice {
                    select im_invoice__new (
                        :invoice_id,		-- invoice_id
                        'im_invoice',		-- object_type
                        now(),			-- creation_date
                        :user_id,		-- creation_user
                        '[ad_conn peeraddr]',	-- creation_ip
                        null,			-- context_id
                        :invoice_nr,		-- invoice_nr
                        :company_id,		-- company_id
                        :provider_id,	-- provider_id
                        :freelance_id,	-- company_contact_id
                        now(),		    -- invoice_date
                        :currency,			-- currency
                        :template_id,	-- invoice_template_id
                        :target_cost_status_id,	-- invoice_status_id
                        :target_cost_type_id,		-- invoice_type_id
                        :default_payment_method_id,	-- payment_method_id
                        :payment_days,		-- payment_days
                        0,			    -- amount
                        to_number(:default_vat,:tax_format),			-- vat
                        to_number(:default_tax,:tax_format),			-- tax
                        :note			-- note
                    )
            }]


            db_dml update_costs "
            update im_costs
            set
                project_id	= :project_id,
                cost_name	= :invoice_nr,
                customer_id	= :company_id,
                cost_nr		= :invoice_id,
                provider_id	= :provider_id,
                cost_status_id	= :target_cost_status_id,
                cost_type_id	= :target_cost_type_id,
                cost_center_id	= :cost_center_id,
                template_id	= :template_id,
                payment_days	= :payment_days,
                vat		= to_number(:default_vat,:tax_format),
                tax		= to_number(:default_tax,:tax_format),
                variable_cost_p = 't',
                currency	= :currency,
                payment_term_id = :payment_term_id,
                vat_type_id     = :vat_type_id
            where
                cost_id = :invoice_id
            "

            # Add the link between the project and the invoice
            set rel_id [db_exec_plsql create_rel "      select acs_rel__new (
                 null,             -- rel_id
                 'relationship',   -- rel_type
                 :project_id,      -- object_id_one
                 :invoice_id,      -- object_id_two
                 null,             -- context_id
                 null,             -- creation_user
                 null             -- creation_ip
          )"]

            lappend created_invoice_ids $invoice_id
            # ---------------------------------------------------------------
            # Create the line items
            # ---------------------------------------------------------------
            set sort_order 0
            set delivery_date ""

            # ---------------------------------------------------------------
            #  Minimum Price handling
            # ---------------------------------------------------------------

            set min_price_task_type_ids [list]

            foreach task_type_id [set task_type_ids(${freelance_id})] {
                set total_rate_info [im_translation_best_rate \
                    -provider_id $provider_id \
                    -task_type_id $task_type_id \
                    -subject_area_id $subject_area_id \
                    -target_language_id $target_language_freelancer($freelance_id) \
                    -source_language_id $source_language_freelancer($freelance_id) \
                    -task_uom_id $task_uom_id \
                    -currency $currency \
                    -ignore_min_price_p 0 \
                    -task_sum $billable_units_total(${task_type_id}_$freelance_id)]
                    
                ds_comment "RATES:: $task_uom_id $task_type_id $total_rate_info [im_category_from_id $target_language_freelancer($freelance_id)] -- [im_category_from_id $subject_area_id]"
                # Split the rate information up into the components
                set total_billable_units [lindex $total_rate_info 0]
                set total_uom_id [lindex $total_rate_info 1]
                set total_rate [lindex $total_rate_info 2]

                   
                # We found a language where the tasks together are below the
                # Minimum Price.
                if {[im_uom_unit] eq $total_uom_id} {
                    if {[info exists rates(${task_type_id}_${total_uom_id}_${freelance_id})]} {
                        set total_rate $rates(${task_type_id}_${total_uom_id}_${freelance_id})
                    }
                    lappend min_price_task_type_ids $task_type_id
                    
                    set material_id [im_material_create_from_parameters -material_uom_id $total_uom_id -debug 1]
                    
                    # Insert the min price
                    
                    set item_id [db_nextval "im_invoice_items_seq"]
                    set task_name "[im_category_from_id $task_type_id]"
                    
                    set insert_invoice_items_sql "
                        INSERT INTO im_invoice_items (
                            item_id, item_name,
                            project_id, invoice_id,
                            item_units, item_uom_id, item_material_id,
                            price_per_unit, currency,
                            sort_order, item_type_id,
                            item_status_id, description, task_id
                        ) VALUES (
                            :item_id, :task_name,
                            :project_id, :invoice_id,
                            :total_billable_units, :total_uom_id, :material_id,
                            :total_rate, :currency,
                            :sort_order, :task_type_id,
                            null, '', null
                        )"
                    
                    db_dml insert_min_price_items $insert_invoice_items_sql
                    
                    incr sort_order
                }
            }
            
            foreach task_id [lsort -unique $po_task_ids($freelance_id)] {
                foreach task_type_id $task_types(${task_id}_${freelance_id}) {
            
                    # We need to create the line items for all task_types and tasks
                    # But only if we really need to create it.....
                    if {[info exists create_line_item(${task_id}_${task_type_id}_${freelance_id})]} {

                        # We can create lines per task
                        set source_language_id $source_language_task($task_id)
                        set target_language_id $target_language_task($task_id)
                        set task_uom_id $uom(${task_id}_${task_type_id}_${freelance_id})
                        set item_units $billable_units_task(${task_id}_${task_type_id}_${freelance_id})
                        set item_name $task_title(${task_id}_${task_type_id}_${freelance_id})
        
                        if {[info exists rates(${task_type_id}_${task_uom_id}_${freelance_id})]} {
                            set rate $rates(${task_type_id}_${task_uom_id}_${freelance_id})
                            set rate_uom_id $task_uom_id
                        } else {
                            # Get the price from the database
                            set rate_list [im_translation_best_rate -provider_id $provider_id -task_type_id $task_type_id -subject_area_id $subject_area($task_id) -target_language_id $target_language_id -source_language_id $source_language_id -task_uom_id $task_uom_id -currency $currency -task_sum $item_units]
                            set rate [lindex $rate_list 2]
                            set rate_uom_id [lindex $rate_list 1]
                            set item_units [lindex $rate_list 0]
                            if {$rate_uom_id ne $task_uom_id} {
                                # Check if we got a price for the rate_uom_id
                                if {[info exists rates(${task_type_id}_${rate_uom_id}_${freelance_id})]} {
                                    set rate $rates(${task_type_id}_${rate_uom_id}_${freelance_id})
                                }
                            }
                        }
    
                        set file_type_id ""
                        set material_id [im_material_create_from_parameters -material_uom_id $task_uom_id]
        
                        # Deal with the end date
                        # Make the last end date the delivery date for the purchase order
                        if {$delivery_date < $task_end_date(${task_id}_${task_type_id}_${freelance_id})} {set delivery_date $task_end_date(${task_id}_${task_type_id}_${freelance_id})}
                        incr sort_order
    
                        if {!(0 == $item_units || "" == $item_units)} {
                            if {[lsearch $min_price_task_type_ids $task_type_id]>-1} {
                                # We already have a line item for this task,
                                # so only add the task with a 0 units and 0 amount
                                set item_units 0
                            }
                            set item_id [db_nextval "im_invoice_items_seq"]
                            set source_invoice_id -1
                            set insert_invoice_items_sql "
                                    INSERT INTO im_invoice_items (
                                            item_id, item_name,
                                            project_id, invoice_id,
                                            item_units, item_uom_id,
                                            price_per_unit, currency,
                                            sort_order, item_type_id,
                                            item_material_id,
                                            item_status_id, description, task_id,
                                            item_source_invoice_id
                                    ) VALUES (
                                            :item_id, :item_name,
                                            :project_id, :invoice_id,
                                            :item_units, :rate_uom_id,
                                            :rate, :currency,
                                            :sort_order, :task_type_id,
                                            :material_id,
                                            null, '', :task_id,
                                            null
                                )"
                            db_dml insert_invoice_items $insert_invoice_items_sql
                        }
                    }
                }
            }

            # Recalculate and update the invoice amount
            im_invoice_update_rounded_amount \
            -invoice_id $invoice_id \
            -discount_perc 0 \
            -surcharge_perc 0

            # Update the delivery_date
            db_dml update_delivery_date "update im_costs set delivery_date = :delivery_date where cost_id = :invoice_id"
        }
        im_audit -object_type "im_invoice" -object_id $invoice_id -action after_create -status_id $target_cost_status_id -type_id $target_cost_type_id
    }
    return "$created_invoice_ids"
}

ad_proc -public im_translation_freelance_company {
    -freelance_id
} {
    Get the company for a freelancer
} {
    return [util_memoize [list im_translation_freelance_company_helper -freelance_id $freelance_id] 3600]
}

ad_proc -public im_translation_freelance_company_helper {
    -freelance_id
} {
    Get the company for a freelancer
} {
    set company_status_ids [im_sub_categories [im_company_status_active_or_potential]]
    return [db_string company "select company_id from acs_rels, im_companies where company_id = object_id_one and object_id_two = :freelance_id and company_status_id in ([template::util::tcl_to_sql_list $company_status_ids]) limit 1" -default [im_company_freelance]]
}

ad_proc -public im_translation_create_project {
    {-company_id:required}
    {-company_contact_id ""}
    {-parent_id ""}
    {-project_name ""}
    {-project_nr ""}
    {-project_type_id ""}
    {-project_status_id ""}
    {-source_language_id:required}
    {-target_language_ids:required}
    {-project_lead_id ""}
    {-final_company ""}
    {-final_company_id ""}
    {-subject_area_id ""}
    {-project_source_id ""}
    -no_callback:boolean
} {
    Create a translation project

    @param no_callback Disable the callback execution during the project creation. Does not apply to the before_project_create callback
} {
    
    callback intranet-translation::before_project_create -project_type_id $project_type_id -company_id $company_id

    # Auto get the project_nr if missing
    if {$project_nr eq ""} {
        set project_nr [im_next_project_nr -customer_id $company_id -parent_id $parent_id]
    }
    
    # Auto set the project_name
    if {$project_name eq ""} {
        set project_name $project_nr
    }


    # ---------------------------------------------------------------
    # Check if the project already exists
    # ---------------------------------------------------------------
    
    set parent_sql "parent_id = :parent_id"
    if {"" == $parent_id} { set parent_sql "parent_id is NULL" }
    set creation_user [auth::get_user_id]

    set dup_sql "
    	select	count(*)
    	from	im_projects
    	where	$parent_sql and
    		(	upper(trim(project_name)) = upper(trim(:project_name)) OR
    			upper(trim(project_nr)) = upper(trim(:project_nr))
		)"

    set project_id 0
    
    if {[db_string duplicates $dup_sql]} {
    	# Check if it is for the same company_id. Then assume a double click
    	set duplicate_company_id [db_string duplicate_comp_id "select company_id from im_projects where $parent_sql and
                    (	upper(trim(project_name)) = upper(trim(:project_name)) OR
    			upper(trim(project_nr)) = upper(trim(:project_nr))
    		)" -default ""]
    
    	if {$company_id ne $duplicate_company_id} {
    	    # Get a new project_nr etc.
    	    set new_project_nr [im_next_project_nr -customer_id $company_id -parent_id $parent_id]
    
    	    # Replace the project_nr in the project_name
    	    regsub -all $project_nr $project_name $new_project_nr project_name
    	    set project_nr $new_project_nr
    	    set project_id 0
    	   
    	} else {
    	    set project_id [db_string duplicate_project_id "select max(project_id) from im_projects where $parent_sql and
                    (	upper(trim(project_name)) = upper(trim(:project_name)) OR
    			upper(trim(project_nr)) = upper(trim(:project_nr))
    		)" -default "0"]
    	}
    }

    # Try to create the project
    if {$project_id eq "0"} {
    	set project_path [string tolower [string trim $project_name]]

    	# Use sensible defaults
    	if {$project_type_id eq ""} {set project_type_id [im_project_type_trans_edit]}
    	if {$project_status_id eq ""} {set project_status_id [im_project_status_open]}
    	set start_date [db_string get_today "select now()::date"]
    	set end_date $start_date
    
        
    	set project_id [im_project::new \
			    -project_name $project_name \
			    -project_nr $project_nr \
			    -project_path $project_path \
			    -company_id $company_id \
			    -parent_id $parent_id \
			    -project_type_id $project_type_id \
			    -project_status_id $project_status_id \
			    -no_callback_p $no_callback_p
		       ]
    }

    if {$project_id eq 0} {
    	ns_log Error "Creation of project $project_name , $project_nr for [im_name_from_id $company_id] failed"
    	return 0
    } else {

        # Add the project Manager
        if {$project_lead_id ne ""} {
            set role_id [im_biz_object_role_project_manager]
            im_biz_object_add_role $project_lead_id $project_id $role_id
        }
        
        # Project source id
        if {$project_source_id eq ""} {
            set project_source_id [im_project_source_normal]
        }
        db_dml update_project "update im_projects set project_lead_id = :project_lead_id, final_company = :final_company, final_company_id = :final_company_id, subject_area_id = :subject_area_id, project_source_id =:project_source_id, start_date = :start_date, end_date = :end_date where project_id = :project_id"
        
        
        # Deal with translation workflows
        set wf_key [db_string wf "select aux_string1 from im_categories where category_id = :project_type_id" -default ""]
        set wf_exists_p [db_string wf_exists "select count(*) from wf_workflows where workflow_key = :wf_key"]
        if {$wf_exists_p} { set workflow_key $wf_key }
        
        if {[exists_and_not_null workflow_key]} {
            # Create a new workflow case (instance)
            set context_key ""
            set case_id [wf_case_new \
                                 $workflow_key \
                                 $context_key \
                                 $project_id \
                                ]
                
            # Determine the first task in the case to be executed and start+finisch the task.
            im_workflow_skip_first_transition -case_id $case_id
        }
        
        # Save the information about the project target languages
        # in the im_target_languages table
        db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"
    
        foreach lang $target_language_ids {
            ns_log Notice "target_language=$lang"
            set sql "insert into im_target_languages values ($project_id, $lang)"
            db_dml insert_im_target_language $sql
            if {[im_table_exists im_freelancers]} {
                im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $lang
            }
        }
        
        # Add the source language as a skill
        if {[im_table_exists im_freelancers]} {
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
        }
        db_dml update_project "update im_projects set source_language_id = :source_language_id where project_id = :project_id"
        
        if {[info exists company_contact_id]} {
            db_dml update_project "update im_projects set company_contact_id = :company_contact_id where project_id = :project_id"
        }
        
        # ---------------------------------------------------------------------
        # Create the directory structure necessary for the project
        # ---------------------------------------------------------------------
        
        # If the filestorage module is installed...
        set fs_installed_p [im_table_exists im_fs_folders]
        if {$fs_installed_p} {
            set fs_dir_create_proc [parameter::get -package_id [im_package_translation_id] -parameter "FilestorageCreateFolderProc"]
            $fs_dir_create_proc $project_id
        }
        
        # Write Audit Trail
        if {!$no_callback_p} {
            im_project_audit -project_id $project_id  -type_id $project_type_id -status_id $project_status_id -action after_create
        }
        
        # -----------------------------------------------------------------
        # Flush caches related to the project's information
        
        util_memoize_flush_regexp "im_project_has_type_helper.*"
        util_memoize_flush_regexp "db_list_of_lists company_info.*"
        
        # -----------------------------------------------------------------
        # Call the "project_create" or "project_update" user_exit
        
        return $project_id
    }
}

ad_proc -public im_translation_update_materials {
} {
    Update the materials based on the current setup
} {
    set dynfield_sql "
        select	*
        from	acs_attributes aa,
            im_dynfield_widgets dw,
            im_dynfield_attributes da
            LEFT OUTER JOIN im_dynfield_layout dl ON (da.attribute_id = dl.attribute_id)
        where	aa.object_type = 'im_material' and
            aa.attribute_id = da.acs_attribute_id and
            da.widget_name = dw.widget_name and
            coalesce(dl.page_url,'default') = 'default'
        order by dl.pos_y, aa.attribute_id
        "
        set param_derefs [db_list_of_lists param_derefs "select attribute_name, deref_plpgsql_function, sql_datatype from ($dynfield_sql) t"]
    
    
    db_foreach trans_materials {select * from im_materials where material_type_id = 9014} {
        set material_name "Translation"
        set old_material_nr "$material_nr"
        foreach row $param_derefs {
            set attribute_name [lindex $row 0]
            set deref_plpgsql_function [lindex $row 1]
            set sql_datatype [lindex $row 2]
    
            # Append the key=value pair, unless the value is NULL
            eval "set val $$attribute_name"
            if {"" != $val} {
            set param_deref [db_string deref "select ${deref_plpgsql_function}(:${attribute_name}::$sql_datatype)" -default ""]
            if {"" != $material_name} { append material_name ", " }
            append material_name $param_deref
            }
        }
        regsub -all { } [string trim [string tolower $material_name]] "" material_nr
        ns_log Notice "Material:: $old_material_nr: $material_name :: $material_nr :: $material_id"
    
        catch {db_dml update "update im_materials set material_name = :material_name, material_nr = :material_nr where material_id = :material_id"}
    
    }
}

ad_proc -public im_translation_task_processing_time {
    -task_ids
    -task_type
} {

    set units_per_day [db_string upd "select aux_int1 from im_categories where lower(category) = lower(:task_type) and category_type = 'Intranet Trans Task Type'" -default ""]
    if {$task_ids eq ""} {
	set days 0
    }
    if {$units_per_day ne ""} {
        set days [db_string tu "select ceil(coalesce(sum(task_units),0)/:units_per_day) from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids])" -default 0]
    } else {
        set days "0"
    }

    return $days
}
    
ad_proc -public im_translation_processing_time {
    {-project_id:required}
    -update:boolean
    -include_pm:boolean
} {
    Calculate the project duration based on categories describing the number of words per 24 hours. Always use working days.
    
    The units per day of a certain task are stored as aux_int1 for the task type associated with aux_string1 of the project_type_id
    
    @param update Update the processing time of the project
    @return Number of working days
} {
    db_1row project_info "select project_type_id, processing_time, aux_string1 as tasks, coalesce((select sum(task_units) from im_trans_tasks tt where tt.project_id = p.project_id),0) as task_units, (select count(language_id) from im_target_languages tl where tl.project_id = p.project_id) as num_langs from im_projects p, im_categories c where c.category_id = p.project_type_id and p.project_id = :project_id"
    
    # If the processing time is smaller then 1 try to get it from the tasks
    if {$processing_time <1} {
        set days 0
        if {$task_units eq ""} {set task_units 0}
        if {$include_pm_p} {lappend tasks "PM"}
        foreach task $tasks {
            set units_per_day [db_string upd "select aux_int1 from im_categories where lower(category) = lower(:task) and category_type = 'Intranet Trans Task Type'" -default ""]
            
            if {$units_per_day ne ""} {
                # Increase the number of days by the units per day rounded up
                if {$task eq "PM"} {
                    set days [expr $days + ceil($task_units/$units_per_day)]
                } else {
                    # For trans, edit and proof we assume languages are worked on in parallel
                    set days [expr $days + ceil($task_units/$units_per_day/$num_langs)]
                }
            }
        }
        
        if {$update_p} {
            if {$days ne 0} {
                db_dml update_project "update im_projects set processing_time = :days where project_id = :project_id"
            }
        }
    } else {
        set days $processing_time
    }
    return $days
}

ad_proc -public im_translation_task_upd {
    {-task_ids:required}
    {-task_type:required}
} {
    Calculate the tasks duration in days based on categories describing the number of words per 24 hours. Always use working days.

    The units per day of a certain task are stored as aux_int1 for the task type.

    @param update Update the processing time of the project
    @return Number of working days
} {
    set units_per_day [db_string upd "select aux_int1 from im_categories where lower(category) = lower(:task_type) and category_type = 'Intranet Trans Task Type'" -default ""]
    if {$task_ids eq ""} {
	return ""
	ad_script_abort
    }
    if {$units_per_day ne ""} {
        set days [db_string tu "select ceil(coalesce(sum(task_units),0)/:units_per_day) from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $task_ids])" -default 0]
    } else {
        set days "0"
    }
    
    # At least one day of processing!
    return $days
}


ad_proc -public im_translation_processing_deadline {
    -start_timestamp
    -days
} {
    Calculate a deadline base on the processing time, ignoring weekends and general absences of the employees group

    @return Timestamp in 'YYYY-MM-DD HH24:MI' format for the deadline
    @start_timestamp Timestamp from which to calculate the deadline
    @days Number of working days for the processing
} {
    if {$days eq "" || $days eq "0.0" || $days eq 0} {
    	return $start_timestamp
    }
    return [util_memoize [list im_translation_processing_deadline_helper -start_timestamp $start_timestamp -days $days] 3600]
}


ad_proc -public im_translation_processing_deadline_helper {
    -start_timestamp
    -days
} {
    Calculate a deadline base on the processing time, ignoring weekends and general absences of the employees group
    
    @return Timestamp in 'YYYY-MM-DD HH24:MI' format for the deadline
    @start_timestamp Timestamp from which to calculate the deadline
    @days Number of working days for the processing
} {

    # Look three times as long in the future to be on the save side
    if {$days eq ""} {
    	return $start_timestamp
    }
    set calc_end_date_days [expr int($days * 3)]
    set calc_end_timestamp [db_string end_date "select to_char(to_timestamp(:start_timestamp,'YYYY-MM-DD HH24:MI') + interval '$calc_end_date_days days', 'YYYY-MM-DD HH24:MI') from dual"]
    set calc_end_date [lindex [split $calc_end_timestamp " "] 0]
    set calc_end_time [lindex [split $calc_end_timestamp " "] 1]
    
    set start_date [lindex [split $start_timestamp " "] 0]

    # We retrieve a list of the upcoming working days for the employees
    set absent_days [im_absence_dates -start_date $start_date -end_date $calc_end_date -exclude_week_days "" -group_ids 463]
    set week_days [im_absence_week_days -start_date $start_date -end_date $calc_end_date -week_day_list "1 2 3 4 5"]
    
    # Loop through the week_days until we run out of processing days
    set counter 0
    
    set end_date $start_date
    while {$counter<=$days} {

        set week_day [lindex $week_days $counter]
    
        # Ignore absence days
        if {[lsearch $absent_days $week_day]<0} {
    
            # it is a valid working day, so use this as the new end date
            set end_date $week_day
        }
        incr counter
    }
    return "$end_date $calc_end_time"
}

ad_proc -public im_translation_update_language_sort_order {} {
    Update the sort order of the translation languages by use
} {
    set sort_order 1

    db_dml reset_sort "update im_categories set sort_order = null where category_type = 'Intranet Translation Language'"
    db_foreach test {
        select sum(count_lang) as count, language_id from (
            select count(source_language_id) as count_lang, source_language_id as language_id from im_projects group by source_language_id
            UNION
            select count(language_id) as count_lang, language_id from im_target_languages group by language_id
        ) languages group by language_id order by count desc
    } {
        if {$language_id ne "" && $count > 10} {
            db_dml update "update im_categories set sort_order = :sort_order where category_id = :language_id"
            incr sort_order
        }
    }
}

ad_proc -public im_translation_update_project_dates {
    {-project_id:required}
    {-start_timestamp ""}
    {-customer_processing_days ""}
    {-task_ids ""}
    -group_languages:boolean
} {
    Update the project start and end date
    
    @param start_timestamp Start Time in the format 'YYYY-MM-DD HH24:MI'
    @param group_languages If set, assume that all tasks for a target_language will be done by the same freelancer
} {
    
    if {$start_timestamp eq ""} {
        # The start date is now.
        set start_timestamp [db_string start_date "select to_char(now(),'YYYY-MM-DD HH24:MI') from dual"]
    }
    
    if {$customer_processing_days eq ""} {
	set customer_processing_days [im_translation_processing_time -include_pm -update -project_id $project_id]
    }

    set end_timestamp [im_translation_processing_deadline -start_timestamp $start_timestamp -days $customer_processing_days]
    
    #ds_comment "End:: $end_timestamp ... $customer_processing_days"

    # Update the project
    db_dml update_project_dates "update im_projects set start_date = to_date(:start_timestamp, 'YYYY-MM-DD HH24:MI'), end_date = to_timestamp(:end_timestamp,'YYYY-MM-DD HH24:MI') where project_id = :project_id"
    
    # ---------------------------------------------------------------
    # Now we can calculate the deadlines for the providers
    # ---------------------------------------------------------------
    
    # Get the list of tasks for the project so we can calculate the deadlines in order
    set tasks [db_string project_info "select aux_string1 from im_projects p, im_categories c where c.category_id = p.project_type_id and p.project_id = :project_id" -default ""]
    
    if {$group_languages_p} {
        foreach target_language_id [im_target_language_ids $project_id] {
            set task_ids [db_list tasks_ids "select task_id from im_trans_tasks where project_id = :project_id and target_language_id = :target_language_id"]
	    if {$task_ids ne ""} {
		set deadline $start_timestamp
		foreach task_type $tasks {
		    # calculate the processing for this task type and update the end dates
		    set processing_days [im_translation_task_processing_time -task_type "$task_type" -task_ids $task_ids]
		    set deadline [im_translation_processing_deadline -start_timestamp $deadline -days $processing_days]
		    db_dml update_deadline "update im_trans_tasks set ${task_type}_end_date = to_timestamp(:deadline,'YYYY-MM-DD HH24:MI') where task_id in ([template::util::tcl_to_sql_list $task_ids])"
		}
	    }
        }
    } else {
        if {$task_ids eq ""} {
            set task_ids [db_list task_ids "select task_id from im_trans_tasks where project_id = :project_id"]
        }
        foreach task_id $task_ids {
            set deadline $start_timestamp
            foreach task_type $tasks {
                # calculate the processing for this task type and update the end dates
                set processing_days [im_translation_task_processing_time -task_type "$task_type" -task_ids $task_id]
                set deadline [im_translation_processing_deadline -start_timestamp $deadline -days $processing_days]
                db_dml update_deadline "update im_trans_tasks set ${task_type}_end_date = to_timestamp(:deadline,'YYYY-MM-DD HH24:MI') where task_id =:task_id"
            }
        }
    }
    ds_comment "End:: updated tasks: $task_ids"
}
