# /packages/intranet-translation/www/trans-tasks/create-purchase-orders.tcl
#
# Copyright (C) 2015 cognovis GmbH
#

ad_page_contract {
    Purpose: Create purchase orders for the freelancers

    @param return_url the url to return to
    @param project_id group id
} {
    return_url
    project_id:integer
    freelance_ids
}

set task_type_list [db_string project_info "select aux_string1 from im_projects p, im_categories c where c.category_id = p.project_type_id and p.project_id = :project_id" -default ""]
set trans_task_types [list]
foreach category [db_list types "select category from im_categories where category_type = 'Intranet Trans Task Type'"] {
    # Check if we have the column, otherwise ignore
    if {[im_column_exists im_trans_tasks ${category}_id]} {
        set trans_type [string tolower $category]
        set trans_task_type_orig($category) $category
        # Check if the configured type is actually in the ones we need for the project
        if {[lsearch $task_type_list $trans_type]>-1} {
            lappend trans_task_types $trans_type
        }
    }
}

# Passed through form vars
set the_form [ns_getform]
for {set i 0} {$i<[ns_set size $the_form]} {incr i} {
    set var_name [ns_set key $the_form $i] 
    foreach type $trans_task_types {
        if {[lsearch [lindex [split $var_name "_"] 0] $type]>-1} {
            set $var_name [ns_set value $the_form $i]
        }
    }
}

# Get the freelancers

foreach freelance_id $freelance_ids {
    foreach type $task_type_list {
	set task_type_id [db_string project_type "select aux_int2 from im_categories where lower(category)=lower(:type) and category_type = 'Intranet Trans Task Type'" -default ""]
	if {$task_type_id eq ""} { 
	    set task_type_id [ad_call_proc_if_exists [im_project_type_${type}]]
	}
        set uom_id [set ${type}_uom.$freelance_id]
        set price [set ${type}_price.$freelance_id]
        if {$uom_id ne "" && $price ne "" && $task_type_id ne "" } {
            set rates(${task_type_id}_${uom_id}_${freelance_id}) $price
        }
    }    
}

im_translation_create_purchase_orders -project_id $project_id -rate_array rates

db_release_unused_handles
ad_returnredirect $return_url
