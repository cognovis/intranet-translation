# /packages/intranet-translation/www/trans-tasks/task-assignments.tcl
#
# Copyright (C) 2003-2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Assign translators, editors and proof readers to every task

    @param project_id the project_id
    @param orderby the display order
    @param show_all_comments whether to show all comments

    @author Guillermo Belcic
    @author frank.bergmann@project-open.com
} {
    project_id:integer
    { orderby "subproject_name" }
    { auto_assigment "" }
    { auto_assigned_words 0 }
}


set task_type_list [db_string project_info "select aux_string1 from im_projects p, im_categories c where c.category_id = p.project_type_id and p.project_id = :project_id" -default ""]
set trans_task_types [list]
foreach category [db_list types "select category from im_categories where category_type = 'Intranet Trans Task Type'"] {
	# Check if we have the column, otherwise ignore
	if {[im_column_exists im_trans_tasks ${category}_id]} {
		set trans_type [string tolower $category]
		set trans_task_type_orig($category) $category
		# Check if the configured type is actually in the ones we need for the project
		if {[lsearch $task_type_list $trans_type]>-1} {
			lappend trans_task_types $trans_type
		}
	}
}


# Deal with the dates
foreach type $trans_task_types {
	set ${type}_end_date ""
	set ${type}_end_dates [list]
}


# ---------------------------------------------------------------
# Deal with auto_id passed through
# ---------------------------------------------------------------
foreach type $trans_task_types {
	set ${type}_auto_id ""
}

# Set the formvars again
set the_form [ns_getform]
for {set i 0} {$i<[ns_set size $the_form]} {incr i} {
	set [ns_set key $the_form $i] [ns_set value $the_form $i]
}


# -------------------------------------------------------------------------
# Security & Default
# -------------------------------------------------------------------------

set user_id [ad_maybe_redirect_for_registration]
if {![im_permission $user_id view_trans_proj_detail]} {
    ad_return_complaint 1 "<li>You don't have sufficient privileges to view this page"
    return
}

set project_nr [db_string project_nr "select project_nr from im_projects where project_id = :project_id" -default ""]
set page_title "$project_nr - [_ intranet-translation.lt_Translation_Assignmen]"
set context_bar [im_context_bar [list /intranet/projects/ "[_ intranet-translation.Projects]"] [list "/intranet/projects/view?project_id=$project_id" "[_ intranet-translation.One_project]"] $page_title]

if {[apm_package_installed_p "intranet-freelance"]} {
    set subject_area_id [db_string subject "select skill_id from im_object_freelance_skill_map where skill_type_id = 2014 and object_id = :project_id limit 1" -default ""]
} else {
    set subject_area_id [db_string subject "select subject_area_id from im_projects where project_id = :project_id" -default ""]
}

set auto_assignment_component_p [parameter::get_from_package_key -package_key intranet-translation -parameter "EnableAutoAssignmentComponentP" -default 0]
set mass_assignment_component_p [parameter::get_from_package_key -package_key intranet-translation -parameter "EnableMassAssignmentComponentP" -default 0]

set return_url [im_url_with_query]

set bgcolor(0) " class=roweven"
set bgcolor(1) " class=rowodd"

# Workflow available?
set wf_installed_p [im_workflow_installed_p]

set date_format "YYYY-MM-DD"

# -------------------------------------------------------------------------
# Auto assign
# -------------------------------------------------------------------------

set error 0

# Check that there is only a single role being assigned
set assigned_roles 0

foreach type $trans_task_types {
	if {[set ${type}_auto_id] > 0} { incr assigned_roles }
}

if {$assigned_roles > 1} {
    incr error
    append errors "<LI>[_ intranet-translation.lt_Please_choose_only_a_]"
}

if {$auto_assigned_words > 0 && $assigned_roles == 0} {
    incr error
    append errors "<LI>[_ intranet-translation.lt_You_havent_selected_a]"
}

if { $error > 0 } {
    ad_return_complaint "[_ intranet-translation.Input_Error]" "$errors"
}

# ---------------------------------------------------------------------
# Get the list of available resources and their roles
# to format the drop-down select boxes
# ---------------------------------------------------------------------

set resource_sql "
select
	r.object_id_two as user_id,
	im_name_from_user_id (r.object_id_two) as user_name,
	im_category_from_id(m.object_role_id) as role
from
	acs_rels r,
	im_biz_object_members m
where
	r.object_id_one=:project_id
	and r.rel_id = m.rel_id
"


# Add all users into a list
set project_resource_list [list]
db_foreach resource_select $resource_sql {
    lappend project_resource_list [list $user_id $user_name $role]
}



# ---------------------------------------------------------------------
# Get the list of available groups
# ---------------------------------------------------------------------

set groups_sql "
select
	g.group_id,
	g.group_name,
	0 as role
from
	groups g,
	im_profiles p
where
	g.group_id = p.profile_id
"


# Add all groups into a list
set group_list [list]
db_foreach group_select $groups_sql {
    lappend group_list [list $group_id $group_name $role]
}


# ---------------------------------------------------------------------
# Select and format the list of tasks
# ---------------------------------------------------------------------

set extra_where ""
if {$wf_installed_p} {
    set extra_where "and
	t.task_id not in (
		select	object_id
		from	wf_cases
	)
"
}

set task_type_from ""
foreach type $trans_task_types {
	append task_type_from "${type}_id,"
	append task_type_from "to_char(${type}_end_date,'YYYY-MM-DD HH24:MI') as ${type}_end_date,"
	append task_type_from "im_email_from_user_id (t.${type}_id) as ${type}_email,"
	append task_type_from "im_name_from_user_id (t.${type}_id) as ${type}_email,"
}

set task_sql "
select
	task_id,
	project_id,
	target_language_id,
	task_name,
	task_filename,
	task_type_id,
	task_status_id,
	description,
	source_language_id,
	task_units,
	billable_units,
	task_uom_id,
	invoice_id,
	quote_id,
	match_x,
	match_rep,
	match100,
	match95,
	match85,
	match75,
	match50,
	match0,
	end_date,
	tm_integration_type_id,
	billable_units_interco,
	match_perf,
	match_cfr,
	match_f95,
	match_f85,
	match_f75,
	match_f50,
	match_lock,
	locked,	$task_type_from
	ptype_cat.aux_int1 as aux_task_type_id,
	im_category_from_id(t.task_uom_id) as task_uom,
	im_category_from_id(t.task_type_id) as task_type,
	im_category_from_id(t.task_status_id) as task_status,
	im_category_from_id(t.target_language_id) as target_language,
	im_category_from_id(t.source_language_id) as source_language,
	now() as today
from
	im_trans_tasks t,
	im_categories ptype_cat
where
	t.project_id=:project_id and
	t.task_status_id <> 372 and
	ptype_cat.category_id = t.task_type_id
	$extra_where
order by
        t.task_name,
        t.target_language_id
"

# ToDo: Remove the DynamicWF tasks


set task_type_list [db_string project_info "select aux_string1 from im_projects p, im_categories c where c.category_id = p.project_type_id and p.project_id = :project_id" -default ""]
set task_colspan [expr [llength $task_type_list] +6]

set task_html "
<form method=POST action=task-assignments-2>
[export_form_vars project_id return_url]
	<table border=0>
	  <tr>
	    <td colspan=$task_colspan class=rowtitle align=center>
	      [_ intranet-translation.Task_Assignments]
	    </td>
	  </tr>
	  <tr>
	    <td class=rowtitle align=center>[_ intranet-translation.Task_Name]</td>
	    <td class=rowtitle align=center>[_ intranet-translation.Source_Language]</td>
	    <td class=rowtitle align=center>[_ intranet-translation.Target_Lang]</td>
	    <td class=rowtitle align=center>[_ intranet-translation.Task_Type]</td>
	    <td class=rowtitle align=center>[_ intranet-translation.Size]</td>
	    <td class=rowtitle align=center>[_ intranet-translation.UoM]</td>\n"

set price_header_html ""
foreach type $task_type_list {
	set type_id [db_string task_type "select category_id from im_categories where lower(category) = :type and category_type = 'Intranet Trans Task Type'"]
	append task_html "<td class=rowtitle align=center>[im_category_from_id $type_id]</td>\n"
	append price_header_html "<td class=rowtitle align=center colspan=2>[im_category_from_id $type_id]</td>\n"
}
append task_html "</tr>"

# We only need to render an Auto-Assign drop-down box for those
# workflow roles with occur in the project.
# So we define a set of counters for each role, that are evaluated
# later in the Auto-Assign-Component.
#
foreach type $task_type_list {
	set ${type}_assignee_ids [list]
    set n_$trans_type 0
}

set ctr 0

set task_list [array names tasks_id]

# Prepare the list of assignees for each task for later processing

set uom_ids [list]
set target_language_ids [list]

# Keep a list of unique task_names, so we can select the task_names for mass assignment
# Sadly we have tasks for each language combination already in the system ....
set task_names [list]

db_foreach select_tasks $task_sql {
    #    ns_log Notice "task_id=$task_id, status_id=$task_status_id"

    # Check if the task_type was set in categories
    if {"" != $aux_task_type_id} { set task_type_id $aux_task_type_id }

    # Determine if this task is auto-assignable or not,
    # depending on the unit of measure (UoM). We currently
    # only exclude Units and Days.
    #
    # 320 Hour
    # 321 Day
    # 322 Unit
    # 323 Page
    # 324 S-Word
    # 325 T-Word
    # 326 S-Line
    # 327 T-Line
    #
    if {320 == $task_uom_id || 323 == $task_uom_id || 324 == $task_uom_id || 325 == $task_uom_id || 326 == $task_uom_id || 327 == $task_uom_id } {
        set auto_assignable_task 1
    } else {
        set auto_assignable_task 0
    }

    # Add the list uom we have in this assignment
    if {[lsearch $uom_ids $task_uom_id]<0} {lappend uom_ids $task_uom_id}
    if {[lsearch $target_language_ids $target_language_id]<0} {lappend target_language_ids $target_language_id}
    # Determine the fields necessary for each task type
    foreach trans_task_type $task_type_list {
		set $trans_task_type 0
    }

    set wf_list [db_string wf_list "select aux_string1 from im_categories where category_id = :task_type_id"]
    if {"" == $wf_list} { set wf_list "other" }

	# Set the task types so we know which actions are supposed to be done in this task
	set task_types($task_id) [list]
    foreach wf $wf_list {
		set $wf 1
		incr n_$wf
		lappend task_types($task_id) $wf
    }

    # introduce spaces after "/" (by "/ ") to allow for graceful rendering
    regsub {/} $task_name "/ " task_name

    append task_html "
	<tr $bgcolor([expr $ctr % 2])>
	<input type=hidden name=task_status_id.$task_id value=$task_status_id>
	<td>$task_name</td>
	<td>$source_language</td>
	<td>$target_language</td>
	<td>$task_type</td>
	<td>$task_units</td>
	<td>$task_uom</td>"

    # here we compare the assigned words, if the task isn't assigned and if
    # the task's words can be assigned to the translator.:

    # Auto-Assign the task/role if the translator_id is NULL (""),
    # and if there are words left to assign
    foreach type $trans_task_types {
	    if {$auto_assignable_task && [set ${type}_id] == "" && [set ${type}_auto_id] > 0 && [set $type] && $auto_assigned_words > $task_units} {
	        set ${type}_id [set ${type}_auto_id]
	        set auto_assigned_words [expr $auto_assigned_words - $task_units]
    	}
	}

    # Render the 4 possible workflow roles to assign
    
	set orig_source_language_id $source_language_id

	foreach type $task_type_list {
	    set ${type}_html ""
	    set this_end_date ""
	    if { [info exists n_${type}] && [set n_${type}] > 0 } {
		    
		    # Check if the assignee exists and if yes, check if we have a purchase Order
		    set assignee_id [set ${type}_id]
		    if {$assignee_id ne ""} {
				set po_created_p [db_0or1row po_created "select i.invoice_id,i.invoice_nr, sum(item_units) as item_units, price_per_unit, company_contact_id, currency from im_invoice_items ii, im_invoices i, im_trans_tasks t where i.invoice_id = ii.invoice_id and t.task_id = :task_id and t.task_id = ii.task_id and company_contact_id = :${type}_id group by i.invoice_id, invoice_nr, price_per_unit, company_contact_id, currency limit 1"]
		  	} else {
			  	set po_created_p 0
		  	}

			set this_end_date [set ${type}_end_date]
			if { $this_end_date eq ""} {
				set this_end_date $end_date
			}
			if { $this_end_date eq ""} {
				set this_end_date $today
			}
				
			if {$po_created_p} {
				append ${type}_html "[im_name_from_id $assignee_id] <input type=hidden name=task_${type}.$task_id value=$assignee_id> <input type=hidden name=${type}_end.$task_id value=$this_end_date>"
			} else {
				append ${type}_html [im_task_user_select -source_language_id $source_language_id -target_language_id $target_language_id task_${type}.$task_id $project_resource_list [set ${type}_id] translator]
		
				append ${type}_html "<br>
	            			<input type=text size=15 id=\"deadline_${type}_${task_id}\" name=\"${type}_end.$task_id\" value=\"${this_end_date}\">
	            			<script>
	            \$(document).ready(function() {
	            	\$('#deadline_${type}_${task_id}').datetimepicker({
	            	  format:'Y-m-d H:i'
	            	});
	            });
	            			</script>
	            "
	        }
#            append ${type}_html "<br><input type=text size=25 maxlength=25 name=${type}_end.$task_id value=\"$this_end_date\">"
            
            
	        if { $assignee_id ne ""} {
	   			# Get the material to identify the correct invoices
	           	# The material needs to be for the actual type of work, not for the whole task type
	        	set file_type_id ""
				set task_type_id [db_string project_type "select aux_int2 from im_categories where lower(category)=lower(:type) and category_type = 'Intranet Trans Task Type'" -default ""]
				if {$task_type_id eq ""} {
				    set task_type_id [ad_call_proc_if_exists [im_project_type_${type}]]
				}
			    set material_id [im_material_create_from_parameters -material_uom_id $task_uom_id -material_type_id [im_material_type_translation]]
	                
				if {$po_created_p} {
	           		set invoice_url [export_vars -base "/intranet-invoices/view" -url {invoice_id return_url}]
				    set total_price [db_string amount "select amount from im_costs where cost_id = :invoice_id"]

	           	    append ${type}_html "<p/><center><a href='$invoice_url'>$invoice_nr</a>:<br />$item_units ($total_price $currency)</center><p/>"
	           	}
	                
				lappend ${type}_assignee_ids $assignee_id
				set ${type}_langs($assignee_id) "${source_language_id}-${target_language_id}"
	                
				# Get the number of units for the type / UOM combination
				if {[lsearch [list trans edit proof] $type]>-1} {
	               	# First multiple the units with the proper cat matrix for this work of the freelancer
	                set assignee_company_id [im_translation_freelance_company -freelance_id $assignee_id]
	
	                set task_units_new [im_trans_trados_matrix_calculate $assignee_company_id $match_x $match_rep $match100 $match95 $match85 $match75 $match50 $match0 \
	                                        $match_perf $match_cfr $match_f95 $match_f85 $match_f75 $match_f50 $locked $type]
	                if {$task_units_new ne 0} {
	                    set task_units $task_units_new
	                }
	            }
	            if {[exists_and_not_null ${type}-${task_uom_id}($assignee_id)]} {
	                set ${type}-${task_uom_id}($assignee_id) [expr $task_units + [set ${type}-${task_uom_id}($assignee_id)]]
	            } else {
	                set ${type}-${task_uom_id}($assignee_id) $task_units
	            }
	        }
	        append task_html "<td>[set ${type}_html]</td>"
	    } else {
	        append task_html "<td><input type=hidden name='task_${type}.$task_id' value=''><input type=hidden name='${type}_end.$task_id' value=''></td>"
	    }
	       
	    # Append the end date to the list so we can prefill for the mass selection
	    if {$this_end_date ne ""} {
		    lappend ${type}_end_dates.${target_language_id} $this_end_date
		}
    }
    append task_html "</tr>"
    
    incr ctr
}

set freelance_ids [list]
set assignee_ids [list]

foreach type $task_type_list {
	set assignee_ids [concat $assignee_ids [set ${type}_assignee_ids]]
}

# Find out the list of freelancers in the assignments
foreach freelancer_id [lsort -unique $assignee_ids] {
    if {![im_user_is_employee_p $freelancer_id] && "" != $freelancer_id} {
        lappend freelance_ids $freelancer_id
    }
}

# ---------------------------------------------------------------
# Render the Purchase Order creation
# ---------------------------------------------------------------

set price_html ""
if {[llength freelance_ids]>0} {
    
    # Add the form for the freelancer prices
    
    set price_html "
    <form method=POST action='create-purchase-orders'>
    [export_form_vars project_id return_url freelance_ids]
    	<table border=0>
    	  <tr>
    	    <td colspan=[expr [llength $task_type_list]*2 +1] class=rowtitle align=center>
    	      [_ intranet-translation.CreatePurchaseOrders]
    	    </td>
    	  </tr>
    	  <tr>
    	    <td class=rowtitle align=center>[_ intranet-core.Assignee]</td>
    	    $price_header_html
    	    </tr>
       "

    foreach freelancer_id $freelance_ids {
        set freelance_company_id [im_translation_freelance_company -freelance_id $freelancer_id]
        set freelance_company_url [export_vars -base "/intranet/companies/view" -url {{company_id $freelance_company_id}}]
        append price_html "
            <tr>
    	    <td><a href='$freelance_company_url'>[im_name_from_user_id $freelancer_id]</a></td>
        "
        
        # Check for each of the assignments
        foreach type $task_type_list {
            
            
            
            if {[lsearch [set ${type}_assignee_ids] $freelancer_id]>-1} {
                set ${type}_uom_ids($freelancer_id) [list]
                foreach uom_id $uom_ids {
                     # Find out of the assignee is assigned to multiple Units of measure
                     if {[info exists ${type}-${uom_id}($freelancer_id)]} {
                         lappend ${type}_uom_ids($freelancer_id) $uom_id
                     }
                }

                switch [llength [set ${type}_uom_ids($freelancer_id)]] {
                    0 {
                        append price_html "<td colspan=2>&nbsp;</td>
                            <input type=hidden name=${type}_price.$freelancer_id value=''></td>
                            <input type=hidden name=${type}_uom.$freelancer_id value=''></td>
                        "
                    }
                    1 {
                        set uom_id [set ${type}_uom_ids($freelancer_id)]
                        set units [set ${type}-${uom_id}($freelancer_id)]

                        # We try to find the correct trans type id. If we have prices maintained though for Trans as well as Trans / Edit, we will most likely not get the proper result, especially not if we have two different Project Types which have
                        # Trans on it's own but are referrenced for the same company.
                        set task_type_id [db_string task "select category_id from im_categories where category_id in (select distinct task_type_id from im_trans_prices where company_id = :freelance_company_id) and aux_string1 = :type and category_type = 'Intranet Project Type' limit 1" -default ""]
                        
                        if {$task_type_id eq ""} {
                            set freelance_company_id [im_company_freelance]
                            set task_type_id [db_string task "select category_id from im_categories where category_id in (select distinct task_type_id from im_trans_prices where company_id = :freelance_company_id) and aux_string1 like '%${type}%' and category_type = 'Intranet Project Type' limit 1" -default ""]
                        }
                        if {$task_type_id eq ""} {
			    set task_type_id [db_string task "select aux_int2 from im_categories where lower(category) = :type and category_type = 'Intranet Trans Task Type' limit 1" -default ""]
                        }

                        # Find out the best price
                        if {$task_type_id eq ""} {
                            set price ""
                        } else {
                            if {[info exists ${type}_langs($freelancer_id)]} {
                                set langs [split [set ${type}_langs($freelancer_id)] "-"]
                                set source_language_id [lindex $langs 0]
                                set target_language_id [lindex $langs 1]
                            } else {
                                set source_language_id ""
                                set target_language_id ""
                            }
                            
                            # Get the price from the database
                            
                            if {$units eq ""} {set units 0}
                            set price_info [im_translation_best_rate -provider_id $freelance_company_id -task_type_id $task_type_id -subject_area_id $subject_area_id -target_language_id $target_language_id -source_language_id $source_language_id -task_uom_id $uom_id -task_sum $units]
                            set price [lindex $price_info 2]
                            set price_uom_id [lindex $price_info 1]
                            if {$uom_id eq $price_uom_id} {
	                            set name_html "$units [im_category_from_id $uom_id]"
                            } else {
	                            set name_html "$units [im_category_from_id $uom_id]<br />
	                            =>[lindex $price_info 0] [im_category_from_id $price_uom_id]"
	                            set uom_id $price_uom_id
                            }
                        }
                        append price_html "
                            <td align=left>$name_html</td>
                            <td><input type=text size=8 name=${type}_price.$freelancer_id value=\"$price\">
                            <input type=hidden name=${type}_uom.$freelancer_id value='$uom_id'></td>
                        "
                    }
                    default {
                        # Ups, we need to show multiple prices....
                        append price_html "<td colspan=2>&nbsp;</td>
                            <input type=hidden name=${type}_price.$freelancer_id value=''></td>
                            <input type=hidden name=${type}_uom.$freelancer_id value=''></td>
                        "
                    }
                }
            } else {
                append price_html "<td colspan=2>&nbsp;</td>
                    <input type=hidden name=${type}_price.$freelancer_id value=''></td>
                    <input type=hidden name=${type}_uom.$freelancer_id value=''></td>
                "
            }
        }
        append price_html "</tr>"
    }
    append price_html "<tr><td><input type=submit name='create' value='[_ intranet-core.Create]'></td><td colspan=4></td></tr></table></form>"
}

append task_html "
</table>
<input type=submit value=Submit>
</form>
"

# Don't show component if there are no tasks
if {$wf_installed_p && !$ctr} { set task_html "" }

# -------------------------------------------------------------------
# Extract the Headers
# for each of the different workflows that might occur in
# the list of tasks of one project
# -------------------------------------------------------------------

# Determine the header fields for each workflow key
# Data Structures:
#	transitions(workflow_key) => [orderd list of transition-name tuples]
#
set wf_header_sql "
	select distinct
	        wfc.workflow_key,
	        wft.transition_key,
		wft.transition_name,
	        wft.sort_order
	from
	        im_trans_tasks t
	        LEFT OUTER JOIN wf_cases wfc ON (t.task_id = wfc.object_id)
	        LEFT OUTER JOIN wf_transitions wft ON (wfc.workflow_key = wft.workflow_key)
	where
	        t.project_id = :project_id
	        and wft.trigger_type not in ('automatic', 'message')
	order by
	        wfc.workflow_key,
	        wft.sort_order
"
db_foreach wf_header $wf_header_sql {
    set trans_key "$workflow_key $transition_key"
    set trans_list [list]
    if {[info exists transitions($workflow_key)]} {
		set trans_list $transitions($workflow_key)
    }
    lappend trans_list [list $transition_key $transition_name]
    ns_log Notice "task-assignments: header: wf=$workflow_key, trans=$transition_key: $trans_list"
    set transitions($workflow_key) $trans_list
}


# -------------------------------------------------------------------
# Build the assignments table
#
# This query extracts all tasks and all of the task assignments and
# stores them in an two-dimensional matrix (implmented as a hash).
# -------------------------------------------------------------------

set wf_assignments_sql "
	select distinct
	        t.task_id,
		wfc.case_id,
	        wfc.workflow_key,
	        wft.transition_key,
	        wft.trigger_type,
	        wft.sort_order,
	        wfca.party_id,
		wfta.deadline,
		to_char(wfta.deadline, :date_format) as deadline_formatted
	from
	        im_trans_tasks t
	        LEFT OUTER JOIN wf_cases wfc ON (t.task_id = wfc.object_id)
	        LEFT OUTER JOIN wf_transitions wft ON (wfc.workflow_key = wft.workflow_key)
		LEFT OUTER JOIN wf_tasks wfta ON (
			wfta.case_id = wfc.case_id
			and wfc.workflow_key = wfta.workflow_key
			and wfta.transition_key = wfta.transition_key
		)
	        LEFT OUTER JOIN wf_case_assignments wfca ON (
	                wfca.case_id = wfc.case_id
			and wfca.role_key = wft.role_key
	        )
	where
	        t.project_id = :project_id
	        and wft.trigger_type not in ('automatic', 'message')
	order by
	        wfc.workflow_key,
	        wft.sort_order
"

db_foreach wf_assignment $wf_assignments_sql {
    set ass_key "$task_id $transition_key"
    set ass($ass_key) $party_id
    set deadl($ass_key) $deadline_formatted

    ns_log Notice "task-assignments: $workflow_key: '$ass_key' -> '$party_id'"
}


# -------------------------------------------------------------------
# Render the assignments table
# -------------------------------------------------------------------

set wf_assignments_render_sql "
	select
		t.*,
		to_char(t.end_date, :date_format) as end_date_formatted,
		wfc.workflow_key,
		im_category_from_id(t.task_uom_id) as task_uom,
		im_category_from_id(t.task_type_id) as task_type,
		im_category_from_id(t.task_status_id) as task_status,
		im_category_from_id(t.target_language_id) as target_language
	from
		im_trans_tasks t,
		wf_cases wfc
	where
		t.project_id = :project_id
		and t.task_id = wfc.object_id
	order by
		wfc.workflow_key,
		t.task_name
"

set ass_html "
<form method=POST action=task-assignments-wf-2>
[export_form_vars project_id return_url]
<table border=0>
"


set ctr 0
set last_workflow_key ""
db_foreach wf_assignment $wf_assignments_render_sql {
    ns_log Notice "task-assignments: ctr=$ctr, wf_key='$workflow_key', task_id=$task_id"

    # Render a new header line for evey type of Workflow
    if {$last_workflow_key != $workflow_key} {
        append ass_html "
	<tr>
	<td class=rowtitle align=center>[_ intranet-translation.Task_Name]</td>
	<td class=rowtitle align=center>[_ intranet-translation.Target_Lang]</td>
	<td class=rowtitle align=center>[_ intranet-translation.Task_Type]</td>
	<td class=rowtitle align=center>[_ intranet-translation.Size]</td>
	<td class=rowtitle align=center>[_ intranet-translation.UoM]</td>\n"

	set transition_list $transitions($workflow_key)
	foreach trans $transition_list {
	    set trans_key [lindex $trans 0]
	    set trans_name [lindex $trans 1]
	    set key "$workflow_key $trans_key"
	    append ass_html "<td class=rowtitle align=center
		>[lang::message::lookup "" intranet-translation.$trans_key $trans_name]</td>\n"
	}
	append ass_html "</tr>\n"
	set last_workflow_key $workflow_key
    }

    append ass_html "
	    <tr $bgcolor([expr $ctr % 2])>
	        <td>
		  $task_name $task_id
		  <input type=hidden name=task_id value=\"$task_id\">
		</td>
	        <td>$target_language</td>
	        <td>$task_type</td>
	        <td><nobr>$task_units</nobr></td>
	        <td><nobr>$task_uom</nobr></td>
    "
    foreach trans $transitions($workflow_key) {

	set trans_key [lindex $trans 0]
	set trans_name [lindex $trans 1]
	set ass_key "$task_id $trans_key"
	set ass_val $ass($ass_key)
	set deadl_val $deadl($ass_key)
	if {"" == $deadl_val} { set deadl_val "$end_date_formatted" }

	append ass_html "<td>\n"
	append ass_html [im_task_user_select -group_list $group_list "assignment.${trans_key}-$task_id" $project_resource_list $ass_val]
	append ass_html "\n"
	append ass_html "<input type=text size=10 name=deadline.${trans_key}-$task_id value=\"$deadl_val\">"
	append ass_html "\n"
    }
    append ass_html "</tr>\n"
    incr ctr
}

append ass_html "
</table>
<input type=submit value=Submit>
</form>
"

# Skip the dynamic workflow component completely if there was
# no dynamic WF task:
#
if {0 == $ctr} { set ass_html "" }


# -------------------------------------------------------------------
# Auto_Assign HTML Component old version
# -------------------------------------------------------------------

set auto_assignment_html_body ""
set auto_assignment_html_header ""

append auto_assignment_html_header "<td class=rowtitle>[_ intranet-translation.Num_Words]</td>\n"
append auto_assignment_html_body "<td><input type=text size=6 name=auto_assigned_words></td>\n"

foreach type $task_type_list {
    if { [info exists n_${type}] && [set n_${type}] > 0 } {
	    append auto_assignment_html_header "<td class=rowtitle>[_ intranet-translation.Trans]</td>\n"
	    append auto_assignment_html_body "<td>[im_task_user_select ${type}_auto_id $project_resource_list "" translator]</td>\n"
	}
}

set auto_assignment_html "
<form action=\"task-assignments\" method=POST>
[export_form_vars project_id return_url orderby]
<table>
<tr>
  <td colspan=5 class=rowtitle align=center>[_ intranet-translation.Auto_Assignment]</td>
</tr>
<tr align=center>
  $auto_assignment_html_header
</tr>
<tr>
  $auto_assignment_html_body
</tr>
<tr>
  <td align=left colspan=5>
    <input type=submit name='auto_assigment' value='[_ intranet-translation.Auto_Assign]'>
  </td>
</tr>
</table>
</form>
"

# No static tasks - no auto assignment...
if {"" == $task_html} { set auto_assignment_html "" }

# -------------------------------------------------------------------
# Mass_Assign HTML Component
# -------------------------------------------------------------------

set mass_assignment_html_body ""
set mass_assignment_html_header ""

append mass_assignment_html_header "<td class=rowtitle>[_ intranet-translation.Target_Lang]</td>\n
$price_header_html"

append mass_assignment_html_header "<td class=rowtitle>[_ intranet-translation.Task_Name]"
set ctr 0
set no_langs [llength $target_language_ids]

foreach target_language_id $target_language_ids {
    append mass_assignment_html_body "<tr><td>[im_category_from_id $target_language_id]</td>\n"
    foreach type $task_type_list {
	if { [info exists n_${type}] && [set n_${type}] > 0 } {
            if {[exists_and_not_null ${type}_end_dates.$target_language_id]} {
                set end_date [lindex [lsort -unique [set ${type}_end_dates.$target_language_id]] 0]
            } else {
                set end_date ""
            }
            append mass_assignment_html_body "<td>[im_task_user_select -source_language_id $orig_source_language_id -target_language_id $target_language_id -with_no_change ${type}_mass.$target_language_id $project_resource_list "" translator]</td>
            <td>
            			<input type=text size=15 id=\"deadline_${type}_${target_language_id}\" name=\"${type}_end_date.$target_language_id\" value=\"${end_date}\">
            			<script>
            \$(document).ready(function() {
            	\$('#deadline_${type}_${target_language_id}').datetimepicker({
            	  format:'Y-m-d H:i'
            	});
            });
            			</script>
            		</td>"
            
        } else {
	        append mass_assignment_html_body "<input type=hidden name=${type}_mass.$target_language_id value=''>
	        <input type=hidden name=${type}_end_date.$target_language_id value=''>"
        }
    }
    incr ctr
    if {$ctr == 1} {
        append mass_assignment_html_body "<td rowspan=$no_langs valign='top'><ul>"
        # Append the task names
        db_foreach task_names {select task_name,min(task_id) as task_id from im_trans_tasks where project_id = :project_id group by task_name} {
            append mass_assignment_html_body "<input type=checkbox name='bulk_file_ids' id='bulk_file_ids,$task_id' value='$task_id'>$task_name<br />"
        }
        append mass_assignment_html_body "</td>"
    }
     append mass_assignment_html_body "</tr>"
}


set task_type_list [array get task_types]
set mass_assignment_html "
<form action=\"task-assignments-mass\" method=POST>
[export_form_vars project_id target_language_ids return_url orderby task_type_list]
<table>
<tr>
  <td colspan=10 class=rowtitle align=center>[_ intranet-translation.Mass_Assignment]</td>
</tr>
<tr align=center>
  $mass_assignment_html_header
</tr>
  $mass_assignment_html_body
<tr>
  <td align=left colspan=5>
    <input type=submit name='mass_assigment' value='[_ intranet-translation.Auto_Assign]'>
  </td>
</tr>
</table>
</form>
"

# No static tasks - no mass assignment...
if {"" == $task_html} { set mass_assignment_html "" }

# -------------------------------------------------------------------
# Project Subnavbar
# -------------------------------------------------------------------

set bind_vars [ns_set create]
ns_set put $bind_vars project_id $project_id
set parent_menu_id [db_string parent_menu "select menu_id from im_menus where label='project'" -default 0]

set sub_navbar [im_sub_navbar \
    -components \
    -base_url "/intranet/projects/view?project_id=$project_id" \
    $parent_menu_id \
    $bind_vars "" "pagedesriptionbar" "project_trans_tasks_assignments"]

