# /packages/intranet-translation/www/trans-tasks/task-assignments-mass.tcl
#
# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Purpose: Takes commands from the /intranet/projects/view
    page and saves changes, deletes tasks and scans for Trados
    files.

    @param return_url the url to return to
    @param project_id group id
} {
    return_url
    project_id:integer
    target_language_ids 
    
    task_type_list
    bulk_file_ids:multiple
}
set trans_task_types [list]
foreach category [db_list types "select category from im_categories where category_type = 'Intranet Trans Task Type'"] {
    # Check if we have the column, otherwise ignore
    if {[im_column_exists im_trans_tasks ${category}_id]} {
        set trans_type [string tolower $category]
        set trans_task_type_orig($category) $category
        # Check if the configured type is actually in the ones we need for the project
        if {[lsearch [db_string project_info "select aux_string1 from im_projects p, im_categories c where c.category_id = p.project_type_id and p.project_id = :project_id" -default ""] $trans_type]>-1} {
            lappend trans_task_types $trans_type
        }
    }
}

# Need to rename the bulk_file_ids as ns_getform only gets the first one.
set bulk_task_ids $bulk_file_ids

# Passed through form vars
set the_form [ns_getform]
for {set i 0} {$i<[ns_set size $the_form]} {incr i} {
    set var_name [ns_set key $the_form $i] 
    foreach type $trans_task_types {
        if {[lsearch [lindex [split $var_name "_"] 0] $type]>-1} {
            set $var_name [ns_set value $the_form $i]
        }
    }
}

array set task_types $task_type_list
set user_id [ad_maybe_redirect_for_registration]


foreach target_language_id $target_language_ids {
    set task_ids [db_list task_ids "select task_id from im_trans_tasks where target_language_id = :target_language_id and project_id = :project_id and task_name in (select task_name from im_trans_tasks where task_id in ([template::util::tcl_to_sql_list $bulk_task_ids]))"]   
    
    foreach task_id $task_ids {
    
        set update_sql_list [list]    
        foreach type [list trans edit proof other] {
            if {[lsearch $task_types($task_id) $type]>-1} {
                set $type [set ${type}_mass.$target_language_id]
                if {[set $type] ne "no_change"} {
                    lappend update_sql_list "${type}_id = :$type"
                    set ${type}_end_date_target [set ${type}_end_date.$target_language_id]
                    if {[set ${type}_end_date_target] ne ""} {
                        lappend update_sql_list "${type}_end_date = :${type}_end_date_target"
                    }
                }
            }
        }

        if {[llength $update_sql_list]>0} {
            set task_workflow_update_sql "
                update im_trans_tasks set
                [join $update_sql_list ","]
                where
                task_id = :task_id
        "
            db_dml update_workflow $task_workflow_update_sql
        }
        # Notify system about the joyful act
        im_user_exit_call trans_task_assign $task_id
        im_audit -object_type "im_trans_task" -action after_update -object_id $task_id
    }
}
db_release_unused_handles
ad_returnredirect $return_url

