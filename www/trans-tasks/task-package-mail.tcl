# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Template for email inclusion
# @author Malte Sussdorff (sussdorff@sussdorff.de)
# @creation-date 2005-06-14

ad_page_contract {
    See if this person is authorized to read the task file,
    guess the MIME type from the original client filename and
    write the binary file to the connection

    @author malte.sussdorff@cognovis.de
    @creation-date 2017-07-08
} {
    type
    user_id:integer
    project_id:integer
    package_file
    {return_url ""}
    {new_status_p 1}
}

set current_user_id [ad_maybe_redirect_for_registration]
set task_id [db_string task_id "select task_id from im_trans_tasks where ${type}_id = :user_id and project_id = :project_id limit 1" -default ""]
set page_title "Send package"

# Get the overall permissions
im_translation_task_permissions $current_user_id $task_id view read write admin

if {!$read} {
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_You_have_insufficient_1]"
    return
}

set mime_type "text/html"

# Somehow when the form is submited the party_ids values became
# only one element of a list, this avoid that problem

set recipients [list]
lappend recipients [list "[party::name -party_id $user_id]</a> ([cc_email_from_party $user_id])" $user_id]
set party_ids $user_id

set form_elements {
    message_id:key
    {cancel_url:text(hidden) {value $return_url}}
    title:text(hidden),optional
    party_ids:text(hidden),optional
    {message_type:text(hidden) {value "email"}}
    {-section "recipients" {legendtext "[_ acs-mail-lite.Recipients]"}}
}

# ---------------------------------------------------------------
# Support for multiple from elements
# ---------------------------------------------------------------

set from_ids $current_user_id
set from_addr [cc_email_from_party $current_user_id]
set from_options [list]
foreach from_id $from_ids {
	db_1row from_info "select im_name_from_id(party_id) as name, email from parties where party_id = :from_id"
	lappend from_options [list $name $email]
}

append form_elements {
    {from_addr:text(select)
	{label "[_ acs-mail-lite.Sender]"}
	{value $from_addr}
	{options $from_options}
    }
}

append form_elements {
	{to:text(checkbox),multiple,optional
	    {label "[_ acs-mail-lite.Recipients]:"}
	    {options  $recipients }
	    {html {checked 1}}
	}
	{to_addr:text(text),optional
	    {label "[_ acs-mail-lite.Recipients]:"}
	    {html {size 56}}
	    {help_text "[_ acs-mail-lite.cc_help]"}
	}
	{recipients:text(hidden)
	    {value $recipients}
	}
	{project_id:text(hidden)
    	{value $project_id}
    }
    {type:text(hidden)
        {value $type}
    }
}

append form_elements {
    {cc_addr:text(text),optional
        {label "[_ acs-mail-lite.CC]:"}
        {html {size 56}}
        {help_text "[_ acs-mail-lite.cc_help]"}
    }
    {-section ""}
}

append form_elements {
    {package_filename:text(inform),optional {label "[_ acs-mail-lite.Associated_files]"} {value $package_filename}}
    {package_file:text(hidden) {value $package_file}}
    {user_id:text(hidden) {value $user_id}}
    {new_status_p:text(hidden) {value $new_status_p}}
}

#  Need to find a nice default value

set content "Please find attached the File"

set content_list [list $content $mime_type]

append form_elements {
	{-section "message" {legendtext "[_ acs-mail-lite.Message]"}}
    {subject:text(text),optional
        {label "[_ acs-mail-lite.Subject]"}
        {html {size 55}}
        {value "Package for Translation"}
    }
    {content_body:text(richtext),optional
        {label "[_ acs-mail-lite.Message]"}
        {html {cols 55 rows 18}}
        {value $content_list}
    }
	{-section ""}
}

if { ![exists_and_not_null action] } {
    set action [ad_conn url]
}

set edit_buttons [list [list [_ acs-mail-lite.Send] send]]
set package_filename [file tail $package_file]

ad_form -action $action \
    -html {enctype multipart/form-data} \
    -name email \
    -cancel_label "[_ acs-kernel.common_Cancel]" \
    -cancel_url $return_url \
    -edit_buttons $edit_buttons \
    -form $form_elements \
    -on_submit {
	
		set signature [db_string signature "select signature from parties where email = :from_addr" -default ""]
		if {$signature ne ""} {
			append content_body [template::util::richtext::get_property html_value $signature]
		}

        # List to store know wich emails recieved the message
        set recipients_addr [list]
                
        # Remove all spaces in cc
        regsub -all " " $cc_addr "" cc_addr
        
        set cc_addr [split $cc_addr ";"]
        set to_addr [split $to_addr ";"]

        foreach party_id $to {
            set email [cc_email_from_party $party_id]
            # Check if the cc_ids is already there
            if {[lsearch $to_addr $email]<0} {
                lappend to_addr $email
            }
        }

        foreach to $to_addr {
            acs_mail_lite::send \
                -send_immediately \
                -to_addr $to \
                -cc_addr $cc_addr \
                -from_addr "$from_addr" \
                -subject "$subject" \
                -body "$content_body" \
                -mime_type $mime_type \
                -object_id $project_id  \
                -filesystem_files [list "$package_file"] \
                -use_sender
        }
        
        util_user_message -html -message "[_ acs-mail-lite.Your_message_was_sent_to]"
        
    } -after_submit {
        if {$new_status_p} {
            db_foreach task "select task_id,task_type_id,task_status_id from im_trans_tasks where ${type}_id = :user_id and project_id = :project_id" {
                
                # Record the upload action
                im_trans_download_action -download_file $package_filename $task_id $task_status_id $task_type_id $user_id
            }
        }
        ad_returnredirect $cancel_url
    }

