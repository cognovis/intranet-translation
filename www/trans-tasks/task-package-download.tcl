
ad_page_contract {
    See if this person is authorized to read the task file,
    guess the MIME type from the original client filename and
    write the binary file to the connection

	@author malte.sussdorff@cognovis.de
    @creation-date 2017-07-08
} {
	type
	user_id:integer
    project_id:integer
    package_file
    {new_status_p 1}
}

set current_user_id [ad_maybe_redirect_for_registration]

set task_id [db_string task_id "select task_id from im_trans_tasks where ${type}_id = :user_id and project_id = :project_id limit 1" -default ""]

# Get the overall permissions
im_translation_task_permissions $current_user_id $task_id view read write admin

if {!$read} {
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_You_have_insufficient_1]"
    return
}

if [file readable $package_file] {
	set package_filename [file tail $package_file]
	if {$new_status_p} {
		db_foreach task "select task_id,task_type_id,task_status_id from im_trans_tasks where ${type}_id = :user_id and project_id = :project_id" {
	       	# Record the upload action
	       	im_trans_download_action -download_file $package_filename $task_id $task_status_id $task_type_id $user_id
	    }
	}
	# Return the file
	set outputheaders [ns_conn outputheaders]
	set package_filename [file tail $package_file]
	ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"$package_filename\""
	ns_returnfile 200 application/zip $package_file
} else {
    ad_return_complaint 1 "<li>[_ intranet-translation.lt_The_specified_file_fi]"
}
