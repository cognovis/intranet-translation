# /packages/intranet-translation/www/trans-tasks/task-assignments-2.tcl
#
# Copyright (C) 2003 - 2009 ]project-open[
#
# All rights reserved. Please check
# http://www.project-open.com/license/ for details.

ad_page_contract {
    Purpose: Takes commands from the /intranet/projects/view
    page and saves changes, deletes tasks and scans for Trados
    files.

    @param return_url the url to return to
    @param project_id group id
} {
    return_url
    project_id:integer

    task_status_id:array
}

set task_type_list [db_string project_info "select aux_string1 from im_projects p, im_categories c where c.category_id = p.project_type_id and p.project_id = :project_id" -default ""]
set trans_task_types [list]
set trans_update_list [list]
foreach category [db_list types "select category from im_categories where category_type = 'Intranet Trans Task Type'"] {
    # Check if we have the column, otherwise ignore
    if {[im_column_exists im_trans_tasks ${category}_id]} {
        set trans_type [string tolower $category]
        set trans_task_type_orig($category) $category
        # Check if the configured type is actually in the ones we need for the project
        if {[lsearch $task_type_list $trans_type]>-1} {
            lappend trans_task_types $trans_type
        }
    }
}

foreach type $trans_task_types {
    lappend trans_update_list "${type}_id = :$type, ${type}_end_date = :${type}_end_date"
}

set trans_update_sql [join $trans_update_list ","]

set task_list [array names task_status_id]

# Passed through form vars
set the_form [ns_getform]
for {set i 0} {$i<[ns_set size $the_form]} {incr i} {
    set var_name [ns_set key $the_form $i]
    set $var_name [ns_set value $the_form $i]
}

set user_id [ad_maybe_redirect_for_registration]



foreach task_id $task_list {
    
    foreach type $trans_task_types {
        if {[info exists task_${type}.$task_id]} {
            set $type [set task_${type}.$task_id]
        	set ${type}_end_date [set ${type}_end.$task_id]
        } 
    }

    set task_workflow_update_sql "
update im_trans_tasks set
$trans_update_sql
where
	task_id=:task_id
"
    db_dml update_workflow $task_workflow_update_sql

    # Notify system about the joyful act
    im_user_exit_call trans_task_assign $task_id
    im_audit -object_type "im_trans_task" -action after_update -object_id $task_id

}

db_release_unused_handles
ad_returnredirect $return_url

